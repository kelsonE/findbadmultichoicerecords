# Knack -  Find bad multiple choice records
For when you have records that do not coincide with your multiple choice options

# How to use!

  1. Clone REPO
  2. Download JSON from Knack Export and put it into project root
  3. Run $ npm init
  4. Edit global variables in getMultipleChoice.js script for your use case



### Tech

You must have the following installed:
* NODE.js
* NPM

