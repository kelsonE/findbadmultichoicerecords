const fetch = require("node-fetch");
const app_id = "5d947dbe8303f8001136cfad";

const jsonData = require("./testJSON.json"); //YOUR JSON FILE
const object = "object_2";
const autoIncrementField = "field_26"; //So you can see what record has the irregularity

getObjectMultiChoice(object, app_id, function (fieldData) {
  findIrregularities(fieldData, function (response) {
    console.log("Irregularities Found: %o", response);
  });
});

function findIrregularities(data, callback) {
  let allFields = data;
  let errors = {};
  //loop through multichoice fields
  for (prop in allFields) {
    //loop through data records matching each prop and sorting those that do not match
    jsonData.records.forEach((record, index) => {
      if (!record[prop]) return;
      if (allFields[prop].options.indexOf(record[prop]) === -1) {
        if (!errors[prop]) {
          errors[prop] = [];
          errors[prop].push({
            irregularity: record[prop],
            autoInc: record[autoIncrementField],
            name: allFields[prop].name,
          });
        } else {
          errors[prop].push({
            name: allFields[prop].name,
            field: record[prop],
            autoInc: record[autoIncrementField],
          });
        }
      }
    });
  }
  typeof callback === "function" && callback(errors);
}

function getObjectMultiChoice(object_path, appId, callback) {
  const url = `https://api.knack.com/v1/applications/${appId}`;
  let objects = {};
  fetch(url)
    .then((response) => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json();
    })
    .then((data) => {
      let metadata = data.application;
      metadata.fields = extractFields(metadata);
      metadata.fields.forEach((fields) => {
        if (object_path === fields.object) {
          if (fields.type === "multiple_choice") {
            objects[fields.key] = {
              options: fields.format.options,
              name: fields.name,
            };
          }
        }
      });
      typeof callback === "function" && callback(objects);
    })
    .catch((error) => {
      console.error(error);
      return null;
    });

  function extractFields(metadata) {
    let fields = [];
    metadata.objects.map((object) => {
      let currentfields = object.fields.map((field) => {
        // add scene key as a view property
        field.object = object.key;
        return field;
      });
      fields = [...fields, ...currentfields];
      return null;
    });
    return fields;
  }
}
